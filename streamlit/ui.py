import streamlit as st
import requests
import pandas as pd
from pandas import DataFrame
import altair as alt
from vega_datasets import data

# interact with FastAPI endpoint
url = 'http://fastapi:8000'
endpoint1 = '/df'
dfdummy = DataFrame(requests.get(url+endpoint1).json()['df'])

endpoint2 = '/oms'
dfoms = DataFrame(requests.get(url+endpoint2).json()['oms'])


# construct UI layout

st.title('Semaforo de recomendaciones de viaje en COVID')

st.write('''Lorem ipsum dolor sit amet, consectetur adipiscing 
elit. Vivamus id urna sed arcu dictum rhoncus. Ut id odio id 
lacus dignissim vehicula interdum non dui. Praesent mollis 
felis at tincidunt convallis. ''')  # description and instructions

countries = alt.topo_feature(data.world_110m.url, 'countries')

c = alt.Chart(countries).mark_geoshape()\
    .encode(color=alt.Color('category:Q',\
        scale=alt.Scale(scheme='redyellowgreen')))\
    .transform_lookup(
        lookup='id',
        from_=alt.LookupData(dfdummy, key='id', fields=['category']))\
    .properties(
        width=500,
        height=300)

st.write(c)

st.write('Introduce el país Destino para ver sus datos.')
paises_oms = dfoms['Name'].unique()

## By country'
pais = st.selectbox('Name', paises_oms)
st.dataframe(dfoms[dfoms['Name'] == pais].transpose())