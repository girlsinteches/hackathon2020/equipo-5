import pandas as pd
import numpy as np
import json
import country_converter as coco

def dummydfcreator():
    df = pd.read_csv('https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/UID_ISO_FIPS_LookUp_Table.csv')
    df = df[['iso3']].drop_duplicates()
    df['category'] = np.random.randint(1, 4, size=len(df))
    df['id'] = df.apply(lambda x: coco.convert(names=x['iso3'], to='ISOnumeric'), axis=1)
    df = df[df.id != 'not found']
    result = df[['id','category']].to_json(orient="records")
    return json.loads(result)

def oms_dfdata():
    oms_df = pd.read_csv('https://covid19.who.int/WHO-COVID-19-global-table-data.csv')
    result = oms_df.to_json(orient="records")
    return json.loads(result)